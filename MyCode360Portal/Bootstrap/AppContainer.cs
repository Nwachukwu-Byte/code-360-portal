﻿
using System;
using MyCode360Portal.Contracts.Services.Data;
using MyCode360Portal.Contracts.Services.General;
using MyCode360Portal.Services.Data;
using MyCode360Portal.Services.General;
using MyCode360Portal.ViewModels;
using MyCode360Portal.Repository;
using MyCode360Portal.Contracts.Repository;


using Autofac;

namespace MyCode360Portal.Bootstrap
{

    /// <summary>
    /// Class acts as application's dependency Injection Container
    /// </summary>
    public class AppContainer
    {
        /// <summary>
        /// Creates, wires dependencies and manages lifetime for a set of components. Most
        ///  instances of Autofac.IContainer are created by a Autofac.ContainerBuilder.
        /// </summary>
        private static IContainer _container;
        /// <summary>
        /// Method registers all our dependencies in an app container
        /// </summary>
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();  // ContainerBuilder class is responsible for registering dependencies

            //  ViewModels
            builder.RegisterType<LoginViewModel>();        // Filling our container with dependencies
            builder.RegisterType<RegistrationViewModel>();
            builder.RegisterType<HomeViewModel>();
            //builder.RegisterType<MainViewModel>();
            //builder.RegisterType<MenuViewModel>();

            // Services - General
            builder.RegisterType<ConnectionService>().As<IConnectionService>(); // Filling our container with dependencies
           // builder.RegisterType<NavigationService>().As<INavigationService>();
            builder.RegisterType<AuthenticationService>().As<IAuthenticationService>();
            builder.RegisterType<DialogService>().As<IDialogService>();
            builder.RegisterType<SettingService>().As<ISettingService>().SingleInstance();

            //General
            builder.RegisterType<GenericRepository>().As<IGenericRepository>();

            _container = builder.Build(); // Build() method creates a container with the 
        }

        /// <summary>
        /// Calls dependencies without a constructor
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public static object Resolve(Type typeName)  // var viewModelType = appcontainer.Resolve(ViewModelType)
        {
            return _container.Resolve(typeName);
        }

        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }
    }
}

