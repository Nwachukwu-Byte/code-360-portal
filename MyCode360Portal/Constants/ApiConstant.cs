﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyCode360Portal.Constants
{
    /// <summary>
    /// Class keeps all the API that would be used in the application in 
    /// one class
    /// </summary>
    public class ApiConstant
    {
        public const string BaseApiUrl = "http://unionfaith.westeurope.cloudapp.azure.com:3600";
        public const string AuthenticateEndpoint = "api/Account/Login";
        public const string RegisterEndpoint = "api/authentication/register";



        //  instead of using static use const string
    }
}
