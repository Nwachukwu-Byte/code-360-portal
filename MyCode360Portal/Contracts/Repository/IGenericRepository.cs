﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyCode360Portal.Contracts.Repository
{
    /// <summary>
    /// Class is responsible for data access and Persistence
    /// </summary>
    public interface IGenericRepository
    {
        // These are a bunch of generic methods for getting, posting, updating, deleting, and Posting and getting data from
        // API


        // GET
        /// <summary>
        /// Makes a request to the API
        /// To Get, API Uri and Authentication Token must be supplied.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="authToken"></param>
        /// <returns></returns>
        Task<T> GetAsync<T>(string uri, string authToken = "");
        // POST
        /// <summary>
        /// Posts to the API
        /// Api uri, Authentication token and data to be posted must be supplied as parameter
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="data"></param>
        /// <param name="authToken"></param>
        /// <returns></returns>
        Task<T> PostAsync<T>(string uri, T data, string authToken);
        // UPDATE

        Task<T> PutAsync<T>(string uri, T data, string authToken = "");
        // DELETE 
        Task DeleteAsync(string uri, string authToken = "");
        // POST AND GET
        Task<R> PostAsync<T, R>(string uri, T data, string authToken = "");
    }
}
