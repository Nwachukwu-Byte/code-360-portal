﻿using MyCode360Portal.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyCode360Portal.Contracts.Services.Data
{
    public interface IAuthenticationService
    {
        /// <summary>
        /// Method registers user
        /// </summary>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<AuthenticationResponse> Register(string name, string email,
          string password);
        /// <summary>
        /// Method logs in user
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<AuthenticationResponse> Authenticate(string userName, string password);

        bool IsUserAuthenticated();
    }
}
