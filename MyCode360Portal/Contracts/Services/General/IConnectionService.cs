﻿using Plugin.Connectivity.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyCode360Portal.Contracts.Services.General
{
    public interface IConnectionService
    {
        /// <summary>
        /// property checks connection status
        /// </summary>
        bool IsConnected { get; }
        // event will be chained to a method that handles a change in connection
        event ConnectivityChangedEventHandler ConnectivityChanged;
    }
}
