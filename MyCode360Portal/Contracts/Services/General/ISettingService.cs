﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyCode360Portal.Contracts.Services.General
{
    public interface ISettingService
    {
        void AddItem(string key, string value);
        string GetItem(string key);

        string UserNameSetting { get; set; }
        string UserIdSetting { get; set; }
    }
}
