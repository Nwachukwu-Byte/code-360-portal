﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyCode360Portal.Contracts.Services.General
{
    public interface IDialogService
    {
        /// <summary>
        /// Method shows a complete display alert
        /// </summary>
        /// <param name="message"></param>
        /// <param name="title"></param>
        /// <param name="buttonLabel"></param>
        /// <returns></returns>
        Task ShowDialog(string message, string title, string buttonLabel);
        /// <summary>
        /// Method only shows a display alert with only a message.
        /// </summary>
        /// <param name="message"></param>
        void ShowToast(string message);
    }
}
