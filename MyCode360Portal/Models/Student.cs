﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyCode360Portal.Models
{
    public class Student
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("applicantId")]
        public long ApplicantId { get; set; }

        [JsonProperty("coursesId")]
        public long CoursesId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("paymentType")]
        public string PaymentType { get; set; }

        [JsonProperty("isLockedOut")]
        public bool IsLockedOut { get; set; }
    }
}
