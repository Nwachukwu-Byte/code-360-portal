﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyCode360Portal.Models
{
    public class Performance_Assessment
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("studentId")]
        public long StudentId { get; set; }

        [JsonProperty("date")]
        public DateTimeOffset Date { get; set; }

        [JsonProperty("grade")]
        public string Grade { get; set; }

        [JsonProperty("remark")]
        public string Remark { get; set; }
    }
}
