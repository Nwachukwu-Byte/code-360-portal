﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyCode360Portal.Models
{
    public class Questions
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("question")]
        public string Question { get; set; }

        [JsonProperty("option1")]
        public string Option1 { get; set; }

        [JsonProperty("option2")]
        public string Option2 { get; set; }

        [JsonProperty("option3")]
        public string Option3 { get; set; }

        [JsonProperty("option4")]
        public string Option4 { get; set; }

        [JsonProperty("answer")]
        public string Answer { get; set; }
    }
}
