﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyCode360Portal.Models
{
    public class Test
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("applicantId")]
        public long ApplicantId { get; set; }

        [JsonProperty("score")]
        public long Score { get; set; }

        [JsonProperty("isPassed")]
        public bool IsPassed { get; set; }
    }
}
