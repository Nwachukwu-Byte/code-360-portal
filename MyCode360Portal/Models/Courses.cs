﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyCode360Portal.Models
{
    public class Courses
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("instructorsId")]
        public long InstructorsId { get; set; }
    }
}
