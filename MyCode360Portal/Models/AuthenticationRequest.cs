﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyCode360Portal.Models
{
    public class AuthenticationRequest
    {
        public string Password { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }
    }
}
