﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyCode360Portal.Models
{
    public class Applicant
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("course")]
        public string Course { get; set; }

        [JsonProperty("isAdmitted")]
        public bool IsAdmitted { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("paymentType")]
        public string PaymentType { get; set; }
    }
}
