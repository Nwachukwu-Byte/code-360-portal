﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyCode360Portal.Models
{
    public class Setting
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("testPassPercentage")]
        public long TestPassPercentage { get; set; }

        [JsonProperty("testTolerancePercentage")]
        public long TestTolerancePercentage { get; set; }

        [JsonProperty("questionsNumber")]
        public long QuestionsNumber { get; set; }

        [JsonProperty("preworkDate")]
        public DateTimeOffset PreworkDate { get; set; }

        [JsonProperty("performancePercentage")]
        public long PerformancePercentage { get; set; }
    }
}
