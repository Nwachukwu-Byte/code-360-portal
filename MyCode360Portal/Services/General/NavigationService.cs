﻿//using MyCode360Portal.Contracts.Services.Data;
//using MyCode360Portal.Contracts.Services.General;
//using MyCode360Portal.ViewModels;
//using MyCode360Portal.ViewModels.Base;
//using MyCode360Portal.Views;
//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Threading.Tasks;
//using Xamarin.Forms;

//namespace MyCode360Portal.Services.General
//{
//    public class NavigationService : INavigationService
//    {
//        private readonly IAuthenticationService _authenticationService;
//        private readonly Dictionary<Type, Type> _mappings;

//        protected Application CurrentApplication => Application.Current;

//        public NavigationService(IAuthenticationService authenticationService)
//        {
//            _authenticationService = authenticationService;
//            _mappings = new Dictionary<Type, Type>();

//            CreatePageViewModelMappings();
//        }








//        protected Type GetPageTypeForViewModel(Type viewModelType)
//        {
//            if (!_mappings.ContainsKey(viewModelType))
//            {
//                throw new KeyNotFoundException($"No map for ${viewModelType} was found on navigation mappings");
//            }

//            return _mappings[viewModelType];
//        }

//        protected Page CreatePage(Type viewModelType, object parameter)
//        {
//            Type pageType = GetPageTypeForViewModel(viewModelType);

//            if (pageType == null)
//            {
//                throw new Exception($"Mapping type for {viewModelType} is not a page");
//            }

//            Page page = Activator.CreateInstance(pageType) as Page;

//            return page;
//        }

//        private void CreatePageViewModelMappings()
//        {

//            _mappings.Add(typeof(LoginViewModel), typeof(LoginView));
//            _mappings.Add(typeof(MainViewModel), typeof(MasterDetailPage1));

//            _mappings.Add(typeof(RegistrationViewModel), typeof(RegistrationView));

//        }
//        /// <summary>
//        /// Method should navigate to Main page after successful login
//        /// </summary>
//        /// <returns></returns>
//        public async Task InitializeAsync()
//        {
//            if (_authenticationService.IsUserAuthenticated())
//            {
//                await NavigateToAsync<MainViewModel>();
//            }
//            else
//            {
//                await NavigateToAsync<LoginViewModel>();
//            }
//        }

//        public Task NavigateToAsync<TViewModel>() where TViewModel : ViewModelBase
//        {
//            return InternalNavigateToAsync(typeof(TViewModel), null);
//        }

//        public Task NavigateToAsync<TViewModel>(object parameter) where TViewModel : ViewModelBase
//        {
//            throw new NotImplementedException();
//        }

//        public Task NavigateToAsync(Type viewModelType)
//        {
//            throw new NotImplementedException();
//        }

//        public Task ClearBackStack()
//        {
//            throw new NotImplementedException();
//        }

//        public Task NavigateToAsync(Type viewModelType, object parameter)
//        {
//            throw new NotImplementedException();
//        }

//        public Task NavigateBackAsync()
//        {
//            throw new NotImplementedException();
//        }

//        public Task RemoveLastFromBackStackAsync()
//        {
//            throw new NotImplementedException();
//        }

//        public Task PopToRootAsync()
//        {
//            throw new NotImplementedException();
//        }

//        protected virtual async Task InternalNavigateToAsync(Type viewModelType, object parameter)
//        {
//            var page = CreatePage(viewModelType, parameter);

//            if (page is MasterDetailPage1 || page is RegistrationView)
//            {
//                CurrentApplication.MainPage = page;
//            }
//            else if (page is LoginView)
//            {
//                CurrentApplication.MainPage = page;
//            }
//            else if (CurrentApplication.MainPage is MasterDetailPage1)
//            {
//                var mainPage = CurrentApplication.MainPage as MasterDetailPage1;

//                if (mainPage.Detail is Code360NavigationPage navigationPage)
//                {
//                    var currentPage = navigationPage.CurrentPage;

//                    if (currentPage.GetType() != page.GetType())
//                    {
//                        await navigationPage.PushAsync(page);
//                    }
//                }
//                else
//                {
//                    navigationPage = new Code360NavigationPage(page);
//                    mainPage.Detail = navigationPage;
//                }

//                mainPage.IsPresented = false;
//            }
//            else
//            {
//                var navigationPage = CurrentApplication.MainPage as Code360NavigationPage;

//                if (navigationPage != null)
//                {
//                    await navigationPage.PushAsync(page);
//                }
//                else
//                {
//                    CurrentApplication.MainPage = new Code360NavigationPage(page);
//                }
//            }

//            await (page.BindingContext as ViewModelBase).InitializeAsync(parameter);
//        }

//    }
//}
