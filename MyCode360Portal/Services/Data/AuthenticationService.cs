﻿using MyCode360Portal.Constants;
using MyCode360Portal.Contracts.Repository;
using MyCode360Portal.Contracts.Services.Data;
using MyCode360Portal.Contracts.Services.General;
using MyCode360Portal.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyCode360Portal.Services.Data
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IGenericRepository _genericRepository;
        private readonly ISettingService _settingsService;
        public AuthenticationService(IGenericRepository genericRepository)
        {
           // _settingsService = settingsService;
            _genericRepository = genericRepository;

        }

        /// <summary>
        /// Method handles registration
        /// </summary>
        /// <param name="email"></param>
        /// <param name="name"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<AuthenticationResponse> Register(string email, string name, string password)
        {
            UriBuilder builder = new UriBuilder(ApiConstant.BaseApiUrl)
            {
                Path = ApiConstant.RegisterEndpoint
            };

            AuthenticationRequest authenticationRequest = new AuthenticationRequest()
            {
                Email = email,
                Name = name,
                Password = password
            };

            return await _genericRepository.PostAsync<AuthenticationRequest, AuthenticationResponse>(builder.ToString(), authenticationRequest);
        }

        public bool IsUserAuthenticated()
        {
            return !string.IsNullOrEmpty(_settingsService.UserIdSetting);
        }

        /// <summary>
        /// Method checks and logs in users
        /// </summary>
        /// <param name="name"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<AuthenticationResponse> Authenticate(string name, string password)
        {
            UriBuilder builder = new UriBuilder(ApiConstant.BaseApiUrl)
            {
                Path = ApiConstant.AuthenticateEndpoint   // Login point

            };

            AuthenticationRequest authenticationRequest = new AuthenticationRequest()
            {
                Name = name,
                Password = password
            };

            return await _genericRepository.PostAsync<AuthenticationRequest, AuthenticationResponse>(builder.ToString(), authenticationRequest);
        }



    }
}
