﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyCode360Portal.ViewModels;

namespace MyCode360Portal.Validation
{
   public static class ValidData
    {
        public static bool IsPresent( string input)
        {
            if (input == "")
            {
                Application.Current.MainPage.DisplayAlert("Validation Error", "Field is required", "Ok");
               

                return false;
            }
            else return true;
        }

        public static bool IsValidEmail( string input)
        {
            if (input.IndexOf("@") == -1 ||
                 input.IndexOf(".") == -1)
            {
                Application.Current.MainPage.DisplayAlert("Validation Error", "Email is not in a valid format", "Ok");
               
                return false;
            }
            else return true;
        }

        public static bool isValidPassword(string input)
        {
            if (input.Length <= 5)
            {
                Application.Current.MainPage.DisplayAlert("Entry Error", "password must be upto six characters long in length",
                    "Ok");
                return false;

            }
            else return true;
        }




    }
}
