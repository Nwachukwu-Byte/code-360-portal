﻿using MyCode360Portal.Contracts.Repository;
using MyCode360Portal.Contracts.Services.Data;
using MyCode360Portal.Contracts.Services.General;
using MyCode360Portal.Repository;
using MyCode360Portal.Services.Data;
using MyCode360Portal.Services.General;
using MyCode360Portal.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyCode360Portal.Views.Helpers
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginView : ContentPage
    {
        public IAuthenticationService AuthenticationService { get; }
        public IDialogService DialogService { get; }

        public IGenericRepository GenericRepository { get; }
        public IConnectionService ConnectionService { get; }
        public LoginView( string userName="",string password="" )
        {
            InitializeComponent();
            GenericRepository = new GenericRepository();
            AuthenticationService = new AuthenticationService(GenericRepository);
            ConnectionService = new ConnectionService();

            BindingContext = new LoginViewModel(this.AuthenticationService,
                this.Navigation,
                this.DialogService,
                ConnectionService,
                userName,password:password );
        }


        //public IAuthenticationService authenticationService { get; }

        //public IDialogService Dialog { get; }


        //public IGenericRepository genericRepository { get; }
        //public IConnectionService connectionService { get; }
        //public Login(string email = "", string password = "", string nameoforg = "")
        //{
        //    InitializeComponent();

        //    genericRepository = new GenericRepository();
        //    authenticationService = new ApiServices(genericRepository);
        //    connectionService = new ConnectionService();

        //    BindingContext = new LoginViewModel(this.authenticationService, this.Navigation, this.Dialog, connectionService, email, password);

        //    Mypassword.IsPassword = true;
        //}
    }
}