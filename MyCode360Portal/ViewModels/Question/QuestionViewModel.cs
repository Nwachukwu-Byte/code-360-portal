﻿using MyCode360Portal.Contracts.Services.Data;
using MyCode360Portal.Contracts.Services.General;
using MyCode360Portal.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyCode360Portal.ViewModels.Question
{
    public class QuestionViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ISettingService _settingsService;
        public INavigation Navigation { get; set; }
        public QuestionViewModel(IAuthenticationService authenticationService,
                                    ISettingService settingService,
                                    INavigation navigation,
                                    IConnectionService connectionService,
                                    IDialogService dialogService) : base(connectionService, dialogService)
        {
            _authenticationService = authenticationService;
            _settingsService = settingService;
            Navigation = navigation;
        }

        //public string Question { get; set; }

        //[JsonProperty("option1")]
        //public string Option1 { get; set; }

        //[JsonProperty("option2")]
        //public string Option2 { get; set; }

        //[JsonProperty("option3")]
        //public string Option3 { get; set; }

        //[JsonProperty("option4")]
        //public string Option4 { get; set; }

        //[JsonProperty("answer")]
        //public string Answer { get; set; }

        private string _question;
        private string _option1;
        private string _option2;
        private string _option3;
        private string _option4;
        private string _answer;

        public string Question
        {
            get => _question;
            set
            {
                _question = value;
                OnPropertyChanged();
            }
        }

        public string Option1
        {
            get => _option1;
            set
            {
                _option1 = value;
                OnPropertyChanged();
            }
        }

        public string Option2
        {
            get => _option2;
            set
            {
                _option2 = value;
                OnPropertyChanged();
            }
        }

        public string Option3
        {
            get => _option3;
            set
            {
                _option3 = value;
                OnPropertyChanged();
            }
        }

        public string Option4
        {
            get => _option4;
            set
            {
                _option4 = value;
                OnPropertyChanged();
            }
        }

        public string Answer
        {
            get => _answer;
            set
            {
                _answer = value;
                OnPropertyChanged();
            }
        }
    }
}
