﻿using MyCode360Portal.Contracts.Services.Data;
using MyCode360Portal.Contracts.Services.General;
using MyCode360Portal.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyCode360Portal.ViewModels.Setting
{
    public class SettingViewModel: ViewModelBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ISettingService _settingsService;
        public INavigation Navigation { get; set; }
        public SettingViewModel(IAuthenticationService authenticationService,
                                    ISettingService settingService,
                                    INavigation navigation,
                                    IConnectionService connectionService,
                                    IDialogService dialogService) : base(connectionService, dialogService)
        {
            _authenticationService = authenticationService;
            _settingsService = settingService;
            Navigation = navigation;
        }


        //[JsonProperty("testPassPercentage")]
        //public long TestPassPercentage { get; set; }

        //[JsonProperty("testTolerancePercentage")]
        //public long TestTolerancePercentage { get; set; }

        //[JsonProperty("questionsNumber")]
        //public long QuestionsNumber { get; set; }

        //[JsonProperty("preworkDate")]
        //public DateTimeOffset PreworkDate { get; set; }

        //[JsonProperty("performancePercentage")]
        //public long PerformancePercentage { get; set; }

        private int _testPercentage;
        private int _testTolerancePercentage;
        private int _numberOfQuestions;
        private DateTimeOffset _preworkDate;
        private int _performancePercentage;

        public int TestPercentage
        {
            get => _testPercentage;
            set
            {
                _testPercentage = value;
                OnPropertyChanged();
            }
        }

        public int TestTolerancePercentage
        {
            get => _testTolerancePercentage;
            set
            {
                _testTolerancePercentage = value;
                OnPropertyChanged();
            }
        }
        public int NumberOfQuestions
        {
            get => _numberOfQuestions;
            set
            {
                _numberOfQuestions = value;
                OnPropertyChanged();
            }
        }

        public DateTimeOffset PreWorkDate
        {
            get => _preworkDate;
            set
            {
                _preworkDate = value;
                OnPropertyChanged();
            }
        }

        public int PerformancePercentage
        {
            get => _performancePercentage;
            set
            {
                _performancePercentage = value;
                OnPropertyChanged();
            }
        }


    }
}
