﻿using MyCode360Portal.Contracts.Services.Data;
using MyCode360Portal.Contracts.Services.General;
using MyCode360Portal.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyCode360Portal.ViewModels.Student
{
    public class StudentViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ISettingService _settingsService;
        public INavigation Navigation { get; set; }
        public StudentViewModel(IAuthenticationService authenticationService,
                                    ISettingService settingService,
                                    INavigation navigation,
                                    IConnectionService connectionService,
                                    IDialogService dialogService) : base(connectionService, dialogService)
        {
            _authenticationService = authenticationService;
            _settingsService = settingService;
            Navigation = navigation;
        }

        //[JsonProperty("applicantId")]
        //public long ApplicantId { get; set; }

        //[JsonProperty("coursesId")]
        //public long CoursesId { get; set; }

        //[JsonProperty("name")]
        //public string Name { get; set; }

        //[JsonProperty("email")]
        //public string Email { get; set; }

        //[JsonProperty("paymentType")]
        //public string PaymentType { get; set; }

        //[JsonProperty("isLockedOut")]
        //public bool IsLockedOut { get; set; }

        private int _applicantId;
        private int _courseId;
        private string _name;
        private string _email;
        private string _paymentType;
        private bool _isLockedOut;

        public int ApplicantId
        {
            get => _applicantId;
            set
            {
                _applicantId = value;
                OnPropertyChanged();
            }
        }

        public int CourseId
        {
            get => _courseId;
            set
            {
                _courseId = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public string Email
        {
            get => _email;
            set
            {
                _email = value;
                OnPropertyChanged();
            }
        }
        public string PaymentType
        {
            get => _paymentType;
            set
            {
                _paymentType = value;
                OnPropertyChanged();
            }
        }

        public bool IsLockedOut
        {
            get => _isLockedOut;
            set
            {
                _isLockedOut = value;
                OnPropertyChanged();
            }
        }
    }
}
