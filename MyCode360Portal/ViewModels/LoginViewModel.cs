﻿using MyCode360Portal.Contracts.Services.Data;
using MyCode360Portal.Contracts.Services.General;
using MyCode360Portal.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using MyCode360Portal.Validation;
using MyCode360Portal.Views;
using MyCode360Portal.Models;

namespace MyCode360Portal.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authenticationService;
      //  private readonly ISettingService _settingsService;
        
        public INavigation Navigation { get; set; }

        private string _userName;
        private string _password;
        private IAuthenticationService authenticationService;
        private IDialogService dialogService;
        private IConnectionService connectionService;



        public LoginViewModel(IAuthenticationService authenticationService, 
            INavigation navigation, 
            IDialogService dialogService,
            IConnectionService connectionService, 
            string userName, string password):base(connectionService,dialogService)
        {
            this.authenticationService = authenticationService;
            Navigation = navigation;
            this.dialogService = dialogService;
            this.connectionService = connectionService;
            UserName = userName;
            Password = password;
        }

        public ICommand LoginCommand => new Command(OnLogin);
      //  public ICommand RegisterCommand => new Command(OnRegister);

        public string UserName
        {
            get => _userName;
            set
            {
                _userName = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }


        private async void OnLogin()
        {
            IsBusy = true;
            if (_connectionService.IsConnected)
            {

                if (IsValidData())
                {
                    try
                    {
                        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
                        authenticationResponse = await _authenticationService.Authenticate(UserName, Password);

                        string authentication = authenticationResponse.Token;

                        if (authenticationResponse != null)
                        {

                            authentication = authenticationResponse.Token;
                            string username = authenticationResponse.Username;
                            var Userrole = authenticationResponse.UserRoles;
                            if (!(String.IsNullOrEmpty(authentication)))
                            {

                                IsBusy = false;
                                await Navigation.PushAsync(new MainView(authentication));
                                Navigation.RemovePage(this.Navigation.NavigationStack[0]); // Ensures user cant return to the LoginView after login

                                //    await Navigation.PopAsync().ConfigureAwait(false);

                            }
                            else
                            {
                                if (authenticationResponse == null)
                                {

                                    await Application.Current.MainPage.DisplayAlert(
                                      "This username/password is invalid",
                                      "Error logging you in",
                                      "OK");

                                }



                            }
                        } }

                    catch (Exception)
                    {
                        await Application.Current.MainPage.DisplayAlert(
                                        "Something went wrong",
                                        "Error logging you in Please check your connection",
                                        "OK");
                        IsBusy = false;

                    }

                }

            }
          
        }

     

       

        private bool IsValidData()
        {
            return ValidData.IsPresent(UserName) &&

                   ValidData.IsPresent(Password) &&
                   ValidData.isValidPassword(Password);
                    

        }

       

        }
    }






    

    

