﻿using JetBrains.Annotations;
using MyCode360Portal.Contracts.Services.General;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MyCode360Portal.ViewModels.Base
{
    public class ViewModelBase
    {
        protected readonly IConnectionService _connectionService;
       
        protected readonly IDialogService _dialogService;

        public ViewModelBase(IConnectionService connectionService,
            IDialogService dialogService)
        {
            _connectionService = connectionService;
          
            _dialogService = dialogService;
        }

        private bool _isBusy;

        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                _isBusy = value;
                OnPropertyChanged(nameof(IsBusy));
            }
        }


        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        //public virtual Task InitializeAsync(object data)
        //{
        //    return Task.FromResult(false);
        //}

        public virtual Task InitializeAsync(object data)
        {
            return Task.FromResult(false);
        }

    }
}
