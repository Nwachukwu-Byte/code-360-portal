﻿using MyCode360Portal.Contracts.Services.Data;
using MyCode360Portal.Contracts.Services.General;
using MyCode360Portal.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyCode360Portal.ViewModels.Test
{
    public class TestViewModel: ViewModelBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ISettingService _settingsService;
        public INavigation Navigation { get; set; }
        public TestViewModel(IAuthenticationService authenticationService,
                                    ISettingService settingService,
                                    INavigation navigation,
                                    IConnectionService connectionService,
                                    IDialogService dialogService) : base(connectionService, dialogService)
        {
            _authenticationService = authenticationService;
            _settingsService = settingService;
            Navigation = navigation;
        }

        //[JsonProperty("title")]
        //public string Title { get; set; }

        //[JsonProperty("applicantId")]
        //public long ApplicantId { get; set; }

        //[JsonProperty("score")]
        //public long Score { get; set; }

        //[JsonProperty("isPassed")]
        //public bool IsPassed { get; set; }

        private string _title;
        private int _applicantId;
        private int _score;
        private bool _isPassed;

        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                OnPropertyChanged();
            }
        }

        public int ApplicantId
        {
            get => _applicantId;
            set
            {
                _applicantId = value;
                OnPropertyChanged();
            }
        }

        public int Score
        {
            get => _score;
            set
            {
                _score = value;
                OnPropertyChanged();
            }
        }

        public bool IsPassed
        {
            get => _isPassed;
            set
            {
                _isPassed = value;
                OnPropertyChanged();
            }
        }
    }
}
