﻿using MyCode360Portal.Contracts.Services.Data;
using MyCode360Portal.Contracts.Services.General;
using MyCode360Portal.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace MyCode360Portal.ViewModels
{
    public class RegistrationViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ISettingService _settingsService;

        private string _name;

        private string _password;
        private string _email;

        public RegistrationViewModel(IConnectionService connectionService,
            INavigationService navigationService, IDialogService dialogService,
            IAuthenticationService authenticationService, ISettingService settingsService)
            : base(connectionService,  dialogService)
        {
            _authenticationService = authenticationService;
            _settingsService = settingsService;
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public string Email
        {
            get => _email;
            set
            {
                _email = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }

       // public ICommand RegisterCommand => new Command(OnRegister);
      //  public ICommand LoginCommand => new Command(OnLogin);

        //private async void OnRegister()
        //{
        //    if (_connectionService.IsConnected)
        //    {
        //        var userRegistered = await
        //            _authenticationService.Register(_email, _name, _password);

        //        if (userRegistered.IsAuthenticated)
        //        {
        //            await _dialogService.ShowDialog("Registration successful", "Message", "OK");
        //            _settingsService.UserIdSetting = userRegistered.User.Id;
        //            await _navigationService.NavigateToAsync<LoginViewModel>();
        //        }
        //    }
        //}

        //private void OnLogin()
        //{
        //    _navigationService.NavigateToAsync<LoginViewModel>();
        //}
    }
}
