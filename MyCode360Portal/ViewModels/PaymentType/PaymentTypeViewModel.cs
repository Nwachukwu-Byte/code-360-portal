﻿using MyCode360Portal.Contracts.Services.Data;
using MyCode360Portal.Contracts.Services.General;
using MyCode360Portal.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyCode360Portal.ViewModels.PaymentType
{
   public  class PaymentTypeViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ISettingService _settingsService;
        public INavigation Navigation { get; set; }
        public PaymentTypeViewModel(IAuthenticationService authenticationService,
                                    ISettingService settingService,
                                    INavigation navigation,
                                    IConnectionService connectionService,
                                    IDialogService dialogService):base(connectionService, dialogService)
        {
            _authenticationService = authenticationService;
            _settingsService = settingService;
            Navigation = navigation;
        }

        private string _name;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }
    }
}
