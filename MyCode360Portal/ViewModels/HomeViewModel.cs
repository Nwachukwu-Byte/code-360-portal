﻿using MyCode360Portal.Contracts.Services.General;
using MyCode360Portal.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyCode360Portal.ViewModels
{
    public class HomeViewModel : ViewModelBase
    {
        //private readonly ICatalogDataService _catalogDataService;
        //private ObservableCollection<Pie> _piesOfTheWeek;

        public HomeViewModel(IConnectionService connectionService,
            INavigationService navigationService,
            IDialogService dialogService
            /*ICatalogDataService catalogDataService*/) : base(connectionService,dialogService)
        {
            //_catalogDataService = catalogDataService;

            //PiesOfTheWeek = new ObservableCollection<Pie>();
        }

        //public ICommand PieTappedCommand => new Command<Pie>(OnPieTapped);
        //public ICommand AddToCartCommand => new Command<Pie>(OnAddToCart);

        //public ObservableCollection<Pie> PiesOfTheWeek
        //{
        //    get => _piesOfTheWeek;
        //    set
        //    {
        //        _piesOfTheWeek = value;
        //        OnPropertyChanged();
        //    }
        //}

        //public override async Task InitializeAsync(object data)
        //{
        //    PiesOfTheWeek = (await _catalogDataService.GetPiesOfTheWeekAsync()).ToObservableCollection();
        //}

        //private void OnPieTapped(Pie selectedPie)
        //{
        //    _navigationService.NavigateToAsync<PieDetailViewModel>(selectedPie);
        //}

        //private async void OnAddToCart(Pie selectedPie)
        //{
        //    MessagingCenter.Send(this, MessagingConstants.AddPieToBasket, selectedPie);
        //    await _dialogService.ShowDialog("Pie added to your cart", "Success", "OK");
        //}

    }
}
