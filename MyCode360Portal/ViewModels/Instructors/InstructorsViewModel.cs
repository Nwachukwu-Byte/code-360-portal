﻿using MyCode360Portal.Contracts.Services.Data;
using MyCode360Portal.Contracts.Services.General;
using MyCode360Portal.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyCode360Portal.ViewModels.Instructors
{
    public class InstructorsViewModel: ViewModelBase
    {
       
        //[JsonProperty("title")]
        //public string Title { get; set; }

        //[JsonProperty("instructorsId")]
        //public long InstructorsId { get; set; }
        private readonly IAuthenticationService _authenticationService;
        private readonly ISettingService _settingService;
        private INavigation Navigation { get; set; }
        public InstructorsViewModel(IAuthenticationService authenticationService,
                                    ISettingService settingService,
                                    INavigation navigation,
                                    IConnectionService connectionService,
                                    IDialogService dialogService) : base(connectionService, dialogService)
        {
            _authenticationService = authenticationService;
            _settingService = settingService;
             Navigation = navigation;

        }

        private string _title;
        private string _instructorId;

        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                OnPropertyChanged();
            }
        }

        public string InstructorId
        {
            get => _instructorId;
            set
            {
                _instructorId = value;
                OnPropertyChanged();
            }
        }
    }
}
