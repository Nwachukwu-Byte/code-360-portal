﻿using MyCode360Portal.Contracts.Services.Data;
using MyCode360Portal.Contracts.Services.General;
using MyCode360Portal.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyCode360Portal.ViewModels.Courses
{
    public class CoursesViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ISettingService _settingService;
        private INavigation navigation;
        public CoursesViewModel(IAuthenticationService authenticationService,
            ISettingService settingService,
            INavigation navigation,
            IConnectionService connectionService,
            IDialogService dialogService
            ): base(connectionService, dialogService)
        {
            _authenticationService = authenticationService;
            _settingService = settingService;
        }

        //[JsonProperty("id")]
        //public int Id { get; set; }

        //[JsonProperty("title")]
        //public string Title { get; set; }

        //[JsonProperty("instructorsId")]
        //public long InstructorsId { get; set; }

        private string _title;
        private string _instructorsId;

        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                OnPropertyChanged();
            }
        }

        public string InstructorId
        {
            get => _instructorsId;
            set
            {
                _instructorsId = value;
                OnPropertyChanged();
            }
        }
    }
}
