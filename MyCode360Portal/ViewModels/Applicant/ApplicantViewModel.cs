﻿using MyCode360Portal.Contracts.Services.Data;
using MyCode360Portal.Contracts.Services.General;
using MyCode360Portal.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyCode360Portal.ViewModels.Applicant
{
    public class ApplicantViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ISettingService _settingService;
        public INavigation navigation;
        public ApplicantViewModel(IAuthenticationService authenticationService,
            ISettingService settingService,
            IConnectionService connectionService,
            IDialogService dialogService) : base(connectionService, dialogService)
        {
            _authenticationService = authenticationService;
            _settingService = settingService;
        }

       
        private string _name;
        private string _email;
        private string _address;
        private string _gender;
        private string _course;
        private bool _isAdmitted;
        private string _password;
        private string _paymentType;



        public string Name 
        { get => _name;
            set 
            {
                _name = value;
                OnPropertyChanged();
            } 
        }
        public string Email
        {
            get => _email;
            set
            {
                _email = value;
                OnPropertyChanged();
            }
        }

        public string Address
        {
            get => _address;
            set
            {
                _address = value;
                OnPropertyChanged();
            }
        }

        public string Gender
        {
            get => _gender;
            set
            {
                _gender = value;
                OnPropertyChanged();
            }
        }

        public string Course
        {
            get => _course;
            set
            {
                _course = value;
                OnPropertyChanged();
            }
        }

        public bool IsAdmitted
        {
            get => _isAdmitted;
            set
            {
                _isAdmitted = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }

        public string PaymentType
        {
            get => _paymentType;
            set
            {
                _paymentType = value;
                OnPropertyChanged();
            }
        }
    }
}
