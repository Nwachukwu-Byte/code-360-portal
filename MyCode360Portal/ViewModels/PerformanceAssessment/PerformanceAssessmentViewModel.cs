﻿using MyCode360Portal.Contracts.Services.Data;
using MyCode360Portal.Contracts.Services.General;
using MyCode360Portal.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyCode360Portal.ViewModels.PerformanceAssessment
{
    public class PerformanceAssessmentViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ISettingService _settingsService;
        public INavigation Navigation { get; set; }
        public PerformanceAssessmentViewModel(IAuthenticationService authenticationService,
                                    ISettingService settingService,
                                    INavigation navigation,
                                    IConnectionService connectionService,
                                    IDialogService dialogService) : base(connectionService, dialogService)
        {
            _authenticationService = authenticationService;
            _settingsService = settingService;
            Navigation = navigation;
        }


        //[JsonProperty("studentId")]
        //public long StudentId { get; set; }

        //[JsonProperty("date")]
        //public DateTimeOffset Date { get; set; }

        //[JsonProperty("grade")]
        //public string Grade { get; set; }

        //[JsonProperty("remark")]
        //public string Remark { get; set; }

        private int _studentId;
        private DateTimeOffset _date;
        private string _grade;
        private string _remark;

        public int StudentId
        {
            get => _studentId;
            set
            {
                _studentId = value;
                OnPropertyChanged();
            }
        }

        public DateTimeOffset Date
        {
            get => _date;
            set
            {
                _date = value;
                OnPropertyChanged();
            }
        }

        public string Grade
        {
            get => _grade;
            set
            {
                _grade = value;
                OnPropertyChanged();
            }
        }

        public string Remark
        {
            get => _remark;
            set
            {
                _remark = value;
                OnPropertyChanged();
            }
        }
    }
}
