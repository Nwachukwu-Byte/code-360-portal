﻿using MyCode360Portal.Bootstrap;
using MyCode360Portal.Contracts.Services.General;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MyCode360Portal.Views.Helpers;

namespace MyCode360Portal
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

           // InitializeApp();
            // InitializeNavigation();
            MainPage = new NavigationPage(new LoginView());
           
        }

        private async Task InitializeNavigation()
        {
            var navigationService = AppContainer.Resolve<INavigationService>();
            await navigationService.InitializeAsync();
        }

        private void InitializeApp()
        {
            AppContainer.RegisterDependencies();

     
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
